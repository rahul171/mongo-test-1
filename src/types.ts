export type EmailAccount = Partial<{
  id: number;
  active: boolean;
  availableQuota: number;
  inProgress: boolean;
  interval: Interval;
  sequences: Sequence[];
}>;

export type Interval = Partial<{
  min: number;
  max: number;
}>;

export type Sequence = Partial<{
  id: number;
  active: boolean;
  priority: number;
  distribution: number;
  schedule: Schedule;
  steps: Step[];
}>;

export type Schedule = Partial<{
  id: number;
  timezone: string;
  timeslots: Timeslot[];
}>;

export type Timeslot = Partial<{
  day: number;
  slots: Slot[];
}>;

export type Slot = Partial<{
  start: number;
  end: number;
}>;

export type Step = Partial<{
  id: number;
  priority: number;
  distribution: number;
  contactsInIterationCount: number;
  remainingContactsInIterationCount: number;
  inProgress: boolean;
  relativeDays: number;
  variants: Variant[];
  sequenceContactHistories: SequenceContactHistory[];
}>;

export type Variant = Partial<{
  id: number;
  active: boolean;
  isLastUsed: boolean;
}>;

export type SequenceContactHistory = Partial<{
  id: number;
  createdAt: Date;
  isUpgraded: boolean;
  lastStepProcessedAt: Date;
}>;
