import {
  EmailAccount,
  Sequence,
  SequenceContactHistory,
  Step,
  Variant,
} from './types';

// 50, 30
// 60, 40
// 60, 40

// 0-0:20, 1:30-2
// 1:30-2, 2-2:10, 5-5:20
// 1:30-2, 5-5:20, 6-6:10

const metadata = {
  emailAccounts: 2,
  sequences: [
    {
      schedule: {
        timezone: 'Asia/Kolkata',
        timeslots: [
          [
            [[0], [0, 20]],
            [[1, 30], [2]],
          ],
          [
            [[0], [0, 20]],
            [[1, 30], [2]],
          ],
          [
            [[0], [0, 20]],
            [[1, 30], [2]],
          ],
          [
            [[0], [0, 20]],
            [[1, 30], [2]],
          ],
          [
            [[0], [0, 20]],
            [[1, 30], [2]],
          ],
          [
            [[0], [0, 20]],
            [[1, 30], [2]],
          ],
          [
            [[0], [0, 20]],
            [[1, 30], [2]],
          ],
        ],
      },
    },
    {
      schedule: {
        timezone: 'Asia/Kolkata',
        timeslots: [
          [
            [
              [1, 30],
              [2, 10],
            ],
            [[5], [5, 20]],
          ],
          [
            [
              [1, 30],
              [2, 10],
            ],
            [[5], [5, 20]],
          ],
          [
            [
              [1, 30],
              [2, 10],
            ],
            [[5], [5, 20]],
          ],
          [
            [
              [1, 30],
              [2, 10],
            ],
            [[5], [5, 20]],
          ],
          [
            [
              [1, 30],
              [2, 10],
            ],
            [[5], [5, 20]],
          ],
          [
            [
              [1, 30],
              [2, 10],
            ],
            [[5], [5, 20]],
          ],
          [
            [
              [1, 30],
              [2, 10],
            ],
            [[5], [5, 20]],
          ],
        ],
      },
    },
    {
      schedule: {
        timezone: 'Asia/Kolkata',
        timeslots: [
          [
            [[1, 30], [2]],
            [[5], [5, 20]],
            [[6], [6, 10]],
          ],
          [
            [[1, 30], [2]],
            [[5], [5, 20]],
            [[6], [6, 10]],
          ],
          [
            [[1, 30], [2]],
            [[5], [5, 20]],
            [[6], [6, 10]],
          ],
          [
            [[1, 30], [2]],
            [[5], [5, 20]],
            [[6], [6, 10]],
          ],
          [
            [[1, 30], [2]],
            [[5], [5, 20]],
            [[6], [6, 10]],
          ],
          [
            [[1, 30], [2]],
            [[5], [5, 20]],
            [[6], [6, 10]],
          ],
          [
            [[1, 30], [2]],
            [[5], [5, 20]],
            [[6], [6, 10]],
          ],
        ],
      },
    },
  ],
  steps: 3,
  variants: 2,
  sequenceContactHistories: [3, 6, 9],
  ratio: [2, 1],
  days: 10,
};

export const emailAccounts = Array<EmailAccount>(metadata.emailAccounts)
  .fill({})
  .map((a1, emailAccountIndex) => {
    const emailAccount: EmailAccount = {};
    emailAccount.id = emailAccountIndex + 1;
    emailAccount.active = true;
    emailAccount.availableQuota = 200;
    emailAccount.inProgress = false;
    emailAccount.interval = {
      min: 60,
      max: 180,
    };
    emailAccount.sequences = Array<Sequence>(metadata.sequences.length)
      .fill({})
      .map((a2, sequenceIndex) => {
        const sequence: Sequence = {};
        sequence.id = sequenceIndex + 1;
        sequence.active = true;
        sequence.priority = sequenceIndex + 1;
        sequence.distribution = 1;
        const scheduleMetadata = metadata.sequences[sequenceIndex].schedule;
        sequence.schedule = {
          id: sequenceIndex,
          timezone: scheduleMetadata.timezone,
          timeslots: scheduleMetadata.timeslots.map((item, index) => ({
            day: index,
            slots: item.map((item1) => ({
              start:
                (item1[0][0] ? item1[0][0] : 0) * 60 * 60 +
                (item1[0][1] ? item1[0][1] : 0) * 60 +
                (item1[0][2] ? item1[0][2] : 0),
              end:
                (item1[0][0] ? item1[0][0] : 0) * 60 * 60 +
                (item1[1][1] ? item1[1][1] : 0) * 60 +
                (item1[1][2] ? item1[1][2] : 0),
            })),
          })),
        };
        sequence.steps = Array<Step>(metadata.steps)
          .fill({})
          .map((a3, stepIndex) => {
            const step: Step = {};
            step.id = sequenceIndex * 100 + stepIndex + 1;
            step.priority = stepIndex + 1;
            step.distribution = stepIndex + 1;
            step.contactsInIterationCount = step.distribution;
            step.remainingContactsInIterationCount =
              step.contactsInIterationCount;
            step.relativeDays = stepIndex;
            step.inProgress = false;
            step.variants = Array<Variant>(metadata.variants)
              .fill({})
              .map((a4, variantIndex) => {
                const variant: Variant = {};
                variant.id =
                  sequenceIndex * 100 * 10 + stepIndex * 10 + variantIndex + 1;
                variant.active = true;
                variant.isLastUsed = false;
                return variant;
              });
            step.sequenceContactHistories = [];
            for (let i = 0; i < metadata.days; i++) {
              const currentDate = new Date(
                Date.now() + i * 24 * 60 * 60 * 1000
              );
              const n1 = Math.floor(
                (metadata.sequenceContactHistories[stepIndex] *
                  metadata.ratio[0]) /
                  (metadata.ratio[0] + metadata.ratio[1])
              );
              for (
                let j = 0;
                j < metadata.sequenceContactHistories[stepIndex];
                j++
              ) {
                const sequenceContactHistory: SequenceContactHistory = {};
                sequenceContactHistory.id =
                  sequenceIndex * 100 * 1000 + stepIndex * 1000 + j + 1;
                if (j < n1) {
                  sequenceContactHistory.isUpgraded = false;
                  sequenceContactHistory.createdAt = new Date(
                    currentDate.getTime() -
                      step.relativeDays * 24 * 60 * 60 * 1000
                  );
                  sequenceContactHistory.lastStepProcessedAt =
                    sequenceContactHistory.createdAt;
                } else {
                  sequenceContactHistory.isUpgraded = true;
                  sequenceContactHistory.createdAt = currentDate;
                  sequenceContactHistory.lastStepProcessedAt = new Date(
                    currentDate.getTime() -
                      step.relativeDays * 24 * 60 * 60 * 1000
                  );
                }
                step.sequenceContactHistories.push(sequenceContactHistory);
              }
            }
            return step;
          });
        return sequence;
      });
    return emailAccount;
  });
