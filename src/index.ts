import { Collection, MongoClient } from 'mongodb';
import {
  EmailAccount,
  Sequence,
  SequenceContactHistory,
  Step,
  Schedule,
  Variant,
  Slot,
  Interval,
  Timeslot,
} from './types';
import { emailAccounts } from './data';

const uri = 'mongodb://localhost';

const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

async function runIt() {
  try {
    await client.connect();

    const database = client.db('test11');
    const collection = database.collection('emailaccounts');

    await abcd(collection);
  } catch (e) {
    console.log(e);
  } finally {
    await client.close();
  }
}

runIt();

async function abcd(c: Collection) {
  // const data = {
  //   id: 1,
  //   active: true,
  //   availableQuota: 10,
  //   inProgress: false,
  //   interval: { min: 0, max: 20 },
  //   sequences: [],
  // };
  //
  // const a = await c.insertOne(data);
  // console.log(a);
  // const a = {
  //   a: new Date(),
  // };
  //
  // await c.insertOne(a);
  // const a = await c.find({}).toArray();
  // console.log(a.map((i) => i.sequences));
  //
  // await c.deleteMany({});
  // await c.insertMany(emailAccounts);
}
